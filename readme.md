SSH Key Manager for an enterprise environment. Designed to be run as a shell.
Uses grant syntax (simliar to SQL) to grant access to machines or groups of
machines, and then pushes the user's SSH keys from one machine to another. 
Runs without LDAP or any other dependencies (excepty Ruby). 
